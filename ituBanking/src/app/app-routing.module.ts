import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainSiteComponent } from './Components/main-site/main-site.component';
import { PaymentSiteComponent } from './Components/payment-site/payment-site.component';
import { MovementsSiteComponent } from './Components/movements-site/movements-site.component';
import { SignSiteComponent } from './Components/sign-site/sign-site.component';
import { InvalidSiteComponent } from './Components/invalid-site/invalid-site.component';
import { EshopSiteComponent } from './Components/eshop-site/eshop-site.component';
import { AdminSiteComponent } from './Components/admin-site/admin-site.component';


const routes: Routes = [
  {path: "sign", component: SignSiteComponent},
  {path: "main-site", component: MainSiteComponent},
  {path: "payments", component: PaymentSiteComponent},
  {path: "movements", component: MovementsSiteComponent},
  {path: "eshop", component: EshopSiteComponent},
  {path: "admin-site", component: AdminSiteComponent},
  {path: '**', component: InvalidSiteComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
