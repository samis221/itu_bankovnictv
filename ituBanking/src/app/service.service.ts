import { Injectable } from '@angular/core';
import { payments, Payments } from "./Data/payments";
import { accounts, Accounts } from './Data/accounts';
import { products } from './Data/products';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  accountId: number = 5;

  constructor() { }

  getPaymentsFor(login: String) {
    var paymentsForLogin = new Array<Payments>();
    for (let i = 0; i < payments.length; i++) {
      if (payments[i].loginSender == login || payments[i].loginReceiver == login) {
        paymentsForLogin.push(payments[i]);
      }
    }

    return paymentsForLogin;
  }

  getAccountFor(login: String) : Accounts {
    for (let i = 0; i < accounts.length; i++) {
      if (login == accounts[i].login) {
        return accounts[i];
      }
    }
    return null;
  }

  getAllAccounts() : Accounts[] {
    return accounts;
  }

  getProducts() {
    return products;
  }

  getAccountBalanceFor(login: string) {
    let sum = 0
    payments.forEach(p => 
    {
      if (p.loginReceiver == login) 
      {
        sum += p.value;
      }
      else if (p.loginSender == login) 
      {
        sum -= p.value;
      }
    });

    return sum;
  }

  addPayment(newPayment : Payments)
  {
    payments.push(newPayment);
  }

  selectLoginsFor(login : String)
  {
    var logins = new Array<String>();
    accounts.forEach(a => {
      if (a.login != login) 
      {
        logins.push(a.login);
      }
    })
  
    return logins;
  }

  addAccount(newAccount : Accounts) {
    newAccount.id = this.accountId;
    this.accountId += 1;
    accounts.push(newAccount);
  }

  updateAccount(updatedAcc : Accounts) {
    for (let i = 0; i < accounts.length; i++) {
      if (updatedAcc.id == accounts[i].id) {
        accounts[i].login = updatedAcc.login;
        accounts[i].password = updatedAcc.password;
        accounts[i].profilePic = updatedAcc.profilePic;
      }
    }
  }

  removeAccount(id: number) {
    for (let i = 0; i < accounts.length; i++) {
      if (id == accounts[i].id) {
        accounts.splice(i, 1);
      }
    }
  }
}
