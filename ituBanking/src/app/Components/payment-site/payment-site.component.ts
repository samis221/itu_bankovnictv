/*
 * Autor: Filip Bali (xbalif00)
 */

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiceService } from 'src/app/service.service';
import { Payments } from 'src/app/Data/payments';
import { Location } from "@angular/common";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-payment-site',
  templateUrl: './payment-site.component.html',
  styleUrls: ['./payment-site.component.css']
})
export class PaymentSiteComponent implements OnInit {

  //Property na které se může psát v html
  form : FormGroup;
  max : number;
  receivers : String[];
  login : string;

  //konstruktor slouží pro dekleraci potřebných objektů
  constructor(private fb : FormBuilder,
              private service : ServiceService,
              private location : Location,
              private route : ActivatedRoute) { }

  //speciální fce angularu
  ngOnInit() {

    //bere login z URL
    this.login = this.route.snapshot.queryParams['login']    
    //vybere loginy možných živatelů kterým můžeme poslat peníze
    this.receivers = this.service.selectLoginsFor(this.login);
    //vybere kolik má přihlášený peněz
    this.max = this.service.getAccountBalanceFor(this.login);
    //definice reaktivní formy
    this.form = this.fb.group(
      {
        loginReceiver : this.fb.control('', [Validators.required]),
        value : this.fb.control(0, [Validators.required, Validators.min(0), Validators.max(this.max)]),
        message : this.fb.control(''),
        varSymbol : this.fb.control('')
      }
    );
  }
  //Funkce pro založení platby
  makePayment()
  {
    //generování dnešního datumu
    let date = new Date();
    //vytvoření objektu platby
    let payment : Payments = {
      loginReceiver : this.form.value.loginReceiver,
      loginSender : this.login,
      value : this.form.value.value,
      message : this.form.value.message,
      varSymbol : this.form.value.varSymbol,
      date : new Date(date.getFullYear(), date.getMonth(), (date.getDate() - 1))
    }
    //přidání platby do servisy
    this.service.addPayment(payment);
    //rollback
    this.location.back();
  }

}
