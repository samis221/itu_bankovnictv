/*
 * Autor: Filip Bali (xbalif00)
 */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from 'src/app/service.service';
import { Accounts } from 'src/app/Data/accounts';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-main-site',
  templateUrl: './main-site.component.html',
  styleUrls: ['./main-site.component.css']
})
export class MainSiteComponent implements OnInit {

  login : string;
  account : Accounts;
  accountBalance : number;

  constructor(private route : ActivatedRoute, 
    private service : ServiceService,
    private _snackBar : MatSnackBar) { }

  //Speciální funkce angularu
  ngOnInit() {
    this.login = this.route.snapshot.queryParams['login'];
    //přes servisu vrací objekt účtu
    this.account = this.service.getAccountFor(this.login);
    //vrací stav účtu
    this.accountBalance = this.service.getAccountBalanceFor(this.login);
  }

  logOut()
  {
    this._snackBar.open("Bye, see you soon :)", "Bye", { duration : 3000 })
  }

}
