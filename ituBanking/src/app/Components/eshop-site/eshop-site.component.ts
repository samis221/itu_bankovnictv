/*
 * Autor: Jan Samanek (xsaman02)
 */

import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/service.service';
import { Products } from 'src/app/Data/products';
import { Payments } from 'src/app/Data/payments';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-eshop-site',
  templateUrl: './eshop-site.component.html',
  styleUrls: ['./eshop-site.component.css']
})
export class EshopSiteComponent implements OnInit {

  products : Products[];
  accountBalance : number;
  login : string;

  constructor(private service : ServiceService,
              private _snackBar : MatSnackBar,
              private route : ActivatedRoute) { }

  ngOnInit() {
    this.products = this.service.getProducts();
    this.login = this.route.snapshot.queryParams["login"];
    this.accountBalance = this.service.getAccountBalanceFor(this.login);
  }

  buyProduct(id : number)
  { 
    let product = this.products.find(a => { if(a.id == id)
                                                return a})
    
    let date = new Date();
    let payment : Payments = {
      loginReceiver: "system",
      loginSender: this.login,
      value: product.cost,
      date: new Date(),
      message: "On day " + date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + " you bought " + product.name,
      varSymbol : 0
    };

    payment.date.setDate(date.getDate() - 1);
  
    this.service.addPayment(payment);
    this.openSnackBar(product.name);
  }

  openSnackBar(name : String) {
    let duration = 2;
    this._snackBar.open("Congrats! You bought yourself " + name, "Thanks!", {
      duration: duration * 2000,
    });
  }
}
