import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

import { ServiceService } from '../../service.service';

@Component({
  selector: 'app-sign-site',
  templateUrl: './sign-site.component.html',
  styleUrls: ['./sign-site.component.css']
})
export class SignSiteComponent implements OnInit {
  hide : Boolean;
  username : String;
  password : String;

  constructor(private _router : Router, private _service : ServiceService) {
    this.hide = true; 
  }

  ngOnInit() { }

  // funkcia pre prihlasenie sa do aplikacie
  onSubmit() {
    let account = this._service.getAccountFor(this.username);
    if (account === null) {
      alert("Username doesn't exist")
      return;
    }

    if (account.password === this.password) {
      this._router.navigate(['/main-site'], { queryParams: {login: account.login} });
    }
    else {
      alert("Wrong password, try again")
      return;
    }   
  }
}
