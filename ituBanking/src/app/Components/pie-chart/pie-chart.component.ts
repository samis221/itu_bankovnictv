/*
 * Autor: Jan Samanek (xsaman02)
 */

import { Component, OnInit, Input } from '@angular/core';
import { Label } from 'ng2-charts';
import { ChartOptions, ChartType } from 'chart.js';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {

   @Input() pieChartData = [];
  pieChartLabels: Label[] = [['Gains', 'Grades'], ['Gains', 'Payments'], ['Losses', 'Grades'],  ['Losses', 'Payments']];
  pieChartLegend = true;
  pieChartType: ChartType = 'pie';
  pieChartColors = [
    {
      backgroundColor: ['rgba(0,200,0.3)', 'rgb(0,255,0,1)', 'rgba(200,0,0,0.3)', 'rgba(255,0,0,0.3)' ],
    },
  ];
  pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'left',
    },
  };

  constructor() { }

  ngOnInit() {
  }

   // events
   public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  changeLegendPosition() {
    this.pieChartOptions.legend.position = this.pieChartOptions.legend.position === 'left' ? 'top' : 'left';
  }

}
