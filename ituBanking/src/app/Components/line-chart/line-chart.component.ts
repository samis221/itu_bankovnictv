/*
 * Autor: Jan Samanek (xsaman02)
 */

import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Label, Color, BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit {

  @Input() lineChartData : ChartDataSets[] = [
    { data: [], label: 'Gains' },
    { data: [], label: 'Losses' },
    { data: [], label: 'Account balance' }
  ]

  @Input() lineChartLabels : Label[];

  chartMode = 0;
  toggleButtonText = "show Gains only"

  lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 1,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  lineChartColors: Color[] = [
    { // dark grey
      backgroundColor: 'transparent',
      borderColor: 'royalblue',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // red
      backgroundColor: 'transparent',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // green
      backgroundColor: 'transparent',
      borderColor: 'green',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  lineChartLegend = true;
  lineChartType = 'line';


  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  constructor() { }

  ngOnInit() {
  }


    // events
    chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    }
  
    chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    }
  
    toggleChart() {
  
      if (this.chartMode == 2) {
        this.chart.hideDataset(0, false);
        this.chart.hideDataset(1, false);
        this.chart.hideDataset(2, false);
        this.chartMode = 0;
        this.toggleButtonText = "show Gains only";
      }
      else if (this.chartMode == 0) {
        this.chart.hideDataset(0, false);
        this.chart.hideDataset(1, true);
        this.chart.hideDataset(2, true);
        this.chartMode = 1;
        this.toggleButtonText = "show Losses only";
      }
      else if (this.chartMode == 1) {
        this.chart.hideDataset(0, true);
        this.chart.hideDataset(1, false);
        this.chart.hideDataset(2, true);
        this.chartMode = 2;
        this.toggleButtonText = "show Gains & Losses";
      }
    }
  
    changeColor() {
      if (this.lineChartColors[2].backgroundColor == `rgba(0, 255, 0, 0.3)`) {
        this.lineChartColors[0].backgroundColor = "transparent";
        this.lineChartColors[1].backgroundColor = "transparent";
        this.lineChartColors[2].backgroundColor = "transparent";
      }
      else {
        this.lineChartColors[0].backgroundColor = `rgba(0, 0, 255, 0.3)`;
        this.lineChartColors[1].backgroundColor = 'rgba(255, 0, 0, 0.3)';
        this.lineChartColors[2].backgroundColor = `rgba(0, 255, 0, 0.3)`;
      }
    }

}
