/*
 * Autor: Jan Samanek (xsaman02)
 */

import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-navigation-buttons',
  templateUrl: './navigation-buttons.component.html',
  styleUrls: ['./navigation-buttons.component.css']
})
export class NavigationButtonsComponent implements OnInit {

  login : string;
  isHidden : boolean = true;

  constructor(private service : ServiceService,
              private router : Router,
              private route: ActivatedRoute,
              private _snackBar : MatSnackBar) { }

  ngOnInit() {
    this.login = this.route.snapshot.queryParams['login'];
  }

  logOut()
  {
    this._snackBar.open("Bye, see you soon :)", "Bye", { duration : 3000 });
  }

  onRouteChange()
  {
    let login = this.route.snapshot.queryParams["login"];
    if ( login == "" || login == null || login == undefined ) 
    {
      this.router.navigate(["/sign"]);
    }

    if (login == "xadmin00") this.isHidden = false;
    else this.isHidden = true;
  }

  get accountBalance()
  {
    return this.service.getAccountBalanceFor(this.login);
  }

  get mainSite()
  {    
    let url = this.router.url.split('?')[0] 
    return url == "/main-site";
  }

}
