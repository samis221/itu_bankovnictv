import { Component, OnInit, Input } from '@angular/core';
import { Accounts } from 'src/app/Data/accounts';
import { ServiceService } from 'src/app/service.service';
import DataGrid from "devextreme/ui/data_grid";


@Component({
  selector: 'app-admin-site',
  templateUrl: './admin-site.component.html',
  styleUrls: ['./admin-site.component.css']
})
export class AdminSiteComponent implements OnInit {

  accountList : Accounts[];
  dataGridInstance: DataGrid;

  constructor( private service: ServiceService ) { }

  ngOnInit() {
    this.accountList = this.service.getAllAccounts();
  }

  saveGridInstance (e) {
      this.dataGridInstance = e.component;
  }

  createNewAccount(event) {
    let newAccount : Accounts = {
      id : null,
      login : event.data.login,
      name : event.data.name,
      password : event.data.password,
      profilePic : 'assets/profile4.jpg'
    }

    this.service.addAccount(newAccount);
    this.accountList = this.service.getAllAccounts();

    var index = this.accountList.map(function(el) {
      return el.id;
    }).indexOf(event.data.id);
    this.accountList.splice(index, 1)
  }

  updateAccount(event) {
    this.service.updateAccount(event.data);
    this.accountList = this.service.getAllAccounts();
  }

  removeAccount(event) {
    this.service.removeAccount(event.data.id);
    this.accountList = this.service.getAllAccounts();
  }
}
