/*
 * Autor: Jan Samanek (xsaman02)
 */

import { Component, OnInit, Input } from '@angular/core';
import { PaymentTableModel } from 'src/app/Data/payments';
import {animate, state, style, transition, trigger} from '@angular/animations';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class TableComponent implements OnInit {

  @Input() selectedPayments = [];

  columnsToDisplay = ['loginReceiver', 'loginSender', 'value', 'date', 'varSymbol'];
  expandedElement: PaymentTableModel | null;


  constructor() { }

  ngOnInit() {
  }

}
