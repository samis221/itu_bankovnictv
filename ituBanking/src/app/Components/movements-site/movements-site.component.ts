/*
 * Autor: Jan Samanek (xsaman02)
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label, Color, BaseChartDirective } from 'ng2-charts';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ServiceService } from 'src/app/service.service';
import { Payments, PaymentTableModel } from 'src/app/Data/payments';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/Shared/datepicker-adapter';
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-movements-site',
  templateUrl: './movements-site.component.html',
  styleUrls: ['./movements-site.component.css'],
  providers: [{ provide: DateAdapter, useClass: AppDateAdapter },
  { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }],
})
export class MovementsSiteComponent implements OnInit {

  form: FormGroup;
  myPayments: Payments[];
  login : string;

  lineChartData: ChartDataSets[] = [
    { data: [], label: 'Gains' },
    { data: [], label: 'Losses' },
    { data: [], label: 'Account balance' }
  ];
  lineChartLabels : Label[] = [];
  pieChartData : number[];

  selectedPayments : PaymentTableModel[] = [];

  constructor(private fb: FormBuilder, private service: ServiceService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    let date = new Date();
    date.setMonth(date.getMonth() - 2);
    this.login = this.route.snapshot.queryParams['login']    


    this.form = this.fb.group({
      startDate: this.fb.control(date),
      endDate: this.fb.control(new Date()),
    })

    this.myPayments = this.service.getPaymentsFor(this.login);
    this.configureDataForCharts()

    this.onChanges()
  }


  onChanges() 
  {
    this.form.valueChanges.subscribe(val => { this.configureDataForCharts() })
  }

  configureDataForCharts() {
    //inicialize new payments
    this.lineChartData = [
      { data: [], label: 'Gains' },
      { data: [], label: 'Losses' },
      { data: [], label: 'Account balance' }
    ];

    this.pieChartData = [0,0,0,0];
    this.lineChartLabels = [];
    this.selectedPayments = [];

    var sum: number = 0;
    for (let i = 0; i < this.myPayments.length; i++) {
      if (this.myPayments[i].date >= this.form.value.startDate && this.myPayments[i].date < this.form.value.endDate) {

        //receiver means account gains money
        if (this.myPayments[i].loginReceiver == this.login) 
        {
          this.gains.data.push(this.myPayments[i].value);
          this.losses.data.push(-0);

          //empty string means it's a gain from grade
          //else means it's a payment
          if (this.myPayments[i].loginSender == "") {
            this.pieChartData[0] += this.myPayments[i].value;
          }
          else
          {
            this.pieChartData[1] += this.myPayments[i].value;
          }

          //If account balance is empty, set first value, else add to sum 
          if (!this.accountBalance.data) {
            sum += this.myPayments[i].value;
            this.accountBalance.data.push(sum);
          }
          else {
            sum += this.myPayments[i].value;
            this.accountBalance.data.push(sum);
          }
        }
        else {
          this.gains.data.push(0);
          this.losses.data.push(-this.myPayments[i].value);

          //empty string means it's a loss from grade
          //else means it's a payment
          if (this.myPayments[i].loginReceiver == "") {
            this.pieChartData[2] -= this.myPayments[i].value;
          }
          else
          {
            this.pieChartData[3] -= this.myPayments[i].value;
          }

          if (!this.accountBalance.data) {
            sum -= this.myPayments[i].value;
            this.accountBalance.data.push(sum);
          }
          else {
            sum -= this.myPayments[i].value
            this.accountBalance.data.push(sum)
          }
        }

        let day = this.myPayments[i].date.getDate() + 1;
        let month = this.myPayments[i].date.getMonth() + 1;
        let year = this.myPayments[i].date.getFullYear();
        let label = day + '-' + month + '-' + year
        this.lineChartLabels.push(label);
        this.addToTable(i, label);

      }
    }
  }

  addToTable(i, label)
  {
    let payment : PaymentTableModel = {
      loginSender : this.myPayments[i].loginSender,
      loginReceiver : this.myPayments[i].loginReceiver,
      value : this.myPayments[i].value,
      message : this.myPayments[i].message,
      varSymbol : this.myPayments[i].varSymbol,
      date : label 
    }
    this.selectedPayments.push(payment);
  }

  get gains() {
    return this.lineChartData[0];
  }

  get losses() {
    return this.lineChartData[1];
  }

  get accountBalance() {
    return this.lineChartData[2];
  }

}
