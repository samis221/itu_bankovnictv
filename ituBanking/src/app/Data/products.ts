export interface Products {

    id : number
    name : String
    cost : number
    image : String
    description : String
}

export const products : Products[] = [
    {id: 0, name: "book", cost: 150000, image: "assets/rychle-sipy.jpg", description: "Velice dlouhý popisek knížky rychlé šípy od J. Foglara což je dětská kniha zejména pro kluky v přepubertálním věku. Smyslem tohoto popisku je zjistit jak se bude text zalamovat a reagovat na dlouhý text."},
    {id: 1, name: "football", cost: 1333, image: "assets/football.jpg", description: "ball for soccer"},
    {id: 2, name: "doll", cost: 1000, image: "assets/doll.jpg", description: "doll princess"},
    {id: 3, name: "toy car", cost: 2500, image: "assets/toy-car.jpg", description: "remote control car"}
]
