export interface Payments {
    loginSender : String
    loginReceiver : String
    value : number
    date : Date
    message : String
    varSymbol : Number
}

export interface PaymentTableModel {
    loginSender : String
    loginReceiver : String
    value : number
    date : String
    message : String
    varSymbol : Number
  }

const logins : String[] = ["xsaman02", "xbalif00", "xhorva11", "xadmin00"];


//*                               5     4    3    2    1
const gradeValues : number[] = [-500, -100, 100, 300, 500];

export const payments : Payments[] = paymentsFunc();

function paymentsFunc()
{
    var payments = new Array<Payments>();

    //Adding classical payments from account to account
    for (let i = 0; i < 100; i++) {

        let indexSender = Math.round((Math.random()) * 100) % 4; 
        let indexReceiver = Math.round((Math.random() * 100)) % 4; 
        let dateYear = 2015 + ((Math.random() * 739) % 4);
        let dateMonth = Math.round((Math.random() * 73) % 13) + 1;
        let dateDay = (Math.round(Math.random() * 123 + 55) % 29) + 1;
        let value = Math.round(Math.random() + 134) * 9892 % 5000;
        let varSymbol = 100000 + (Math.random() * 420 + 33 * 0.35) % 99999;

        payments.push(
            {loginSender : logins[indexSender],
             loginReceiver : logins[indexReceiver], 
             value : value, 
             date : new Date(dateYear, dateMonth, dateDay), 
             message : i + ". Message", 
             varSymbol : varSymbol}
        )
    }

    for (let i = 0; i < 100; i++) {
 
        let sender : String;
        let receiver : String;
        let dateYear = 2015 + ((Math.random() * 739) % 5);
        let dateMonth = ((Math.random() * 73) % 13) + 1;
        let dateDay = ((Math.random() * 123 + 55) % 29) + 1;        
        let value = gradeValues[Math.round((Math.random() * 100) % 4)];
        let varSymbol = 100000 + (Math.random() * 420 + 33 * 0.35) % 99999;
        if (value < 0) 
        {
            sender = logins[Math.round((Math.random() * 100)) % 4];
            value *= -1;
            receiver = "";  
        }
        else
        {
            sender = "";  
            receiver = logins[Math.round((Math.random() * 100)) % 4];
        }

        payments.push(
            {loginSender : sender,
             loginReceiver : receiver, 
             value : value, 
             date : new Date(dateYear, dateMonth, dateDay), 
             message : i + ". Grade", 
             varSymbol : varSymbol}
        )
    }

    payments.sort((a, b) => (a.date > b.date) ? 1 : -1)

    return payments;
}

// export const payments : Payments[]
