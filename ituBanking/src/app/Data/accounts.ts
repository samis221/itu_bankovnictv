export interface Accounts {
    id : Number
    login : String
    name : String
    password : String
    profilePic : String
}


export const accounts : Accounts[]= [
    {id: 1, login:"xsaman02", name:"Šamánek Jan", password:"a", profilePic: "assets/profile1.png"},
    {id: 2, login:"xbalif00", name:"Filip Bali", password:"a", profilePic: "assets/profile2.png"},
    {id: 3, login:"xhorva11", name:"Jozef Horvát", password:"a", profilePic: "assets/profile3.png"},
    {id: 4, login:"xadmin00", name:"Jitka Adminová", password:"a", profilePic: "assets/profile4.jpg"}
]

export let accountId : Number = 5