// Hlavní komponenta každé aplikace je app.component
// controller.ts, template.html
// Každá komponenta je definována stejnojmenou třídou.

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

//Dekorátor třídy pro určení metadat třídy.
@Component({
  selector: 'app-root', //název, kterým můžou ostatní komponenty volat tuto komponentu
  templateUrl: './app.component.html', //definice url pro .html soubor
  styleUrls: ['./app.component.css'] //definice url pro .css soubory
})
//                                  Tyto funkce určují speciální angularovské funkce, které se vykonají v čase podle názvu
export class AppComponent implements OnInit{
  
  //property v rámci třídy. Na tyto proměné se dá dotazovat v .html souboru
  title = 'ituBanking';

  //constructor spíše pro definici potřebných objektů.
  constructor(private _router : Router){}

  //Speciální výše zmíněná funkce. 
  ngOnInit(){}

  //getter
  get signTable()
  {
    return this._router.url == "/sign";
  }

}
