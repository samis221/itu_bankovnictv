//Soubor pro definování a importování potřebných souborů a knihoven
//importují se zde i naše ostatní komponenty aby jsme je mohli volat

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SignSiteComponent } from './Components/sign-site/sign-site.component';
import { MainSiteComponent } from './Components/main-site/main-site.component';
import { PaymentSiteComponent } from './Components/payment-site/payment-site.component';
import { NavigationButtonsComponent } from './Components/navigation-buttons/navigation-buttons.component';
import { MovementsSiteComponent } from './Components/movements-site/movements-site.component';
import { InvalidSiteComponent } from './Components/invalid-site/invalid-site.component';

import { MatToolbarModule } from "@angular/material/toolbar";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { MatTooltipModule } from "@angular/material/tooltip";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatInputModule } from "@angular/material/input";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatNativeDateModule } from '@angular/material/core';
import { MatTableModule } from '@angular/material/table';
import {MatCheckboxModule} from '@angular/material/checkbox';

import { ChartsModule } from 'ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceService } from './service.service';
import { PieChartComponent } from './Components/pie-chart/pie-chart.component';
import { LineChartComponent } from './Components/line-chart/line-chart.component';
import { TableComponent } from './Components/table/table.component';
import { EshopSiteComponent } from './Components/eshop-site/eshop-site.component';
import { MatSelectModule } from '@angular/material';
import { AdminSiteComponent } from './Components/admin-site/admin-site.component';

import { DxDataGridModule, DxButtonModule } from 'devextreme-angular';

//Přidávání našich komponent a souborů do NgModule
@NgModule({
  declarations: [
    AppComponent,
    SignSiteComponent,
    MainSiteComponent,
    PaymentSiteComponent,
    NavigationButtonsComponent,
    MovementsSiteComponent,
    InvalidSiteComponent,
    PieChartComponent,
    LineChartComponent,
    TableComponent,
    EshopSiteComponent,
    AdminSiteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
    MatGridListModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,
    MatFormFieldModule,
    MatCardModule,
    MatSelectModule,
    MatSnackBarModule,
    ChartsModule,
    MatCheckboxModule,
    DxDataGridModule,
    DxButtonModule
  ],
  providers: [MatDatepickerModule, ServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
