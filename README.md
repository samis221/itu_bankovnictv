itu bankovnictvi

Manual na zprovoznění projektu:

Projekt je vytvořen v Angular 8


#############################
Dostupná ip adresa projektu
http://167.172.183.1/

Existující účty pro přihlášení do aplikace
login : xsaman02
heslo : a

login : xhorva11
heslo : a

login : xbalif00
heslo : a

login : xadmin00
heslo : a
#############################

Projekt není odevzdán jako celek se všemi potřebnými knihovnami.
Jsou odevzdané pouze části aplikace které jsme samostatně vytvořili. Nebo které jsou potřebné pro instalaci.

Kroky pro zprovoznění jsou následující:

1.  Je potřeba mít na svém zařízení nainstalovaný npm (node package manager)
2.  Skrz npm pak naistalujeme angular klienta pomocí příkazu  ```npm install -g @angular/cli``` který nainstaluje nejnovější verzi angularu jako globální verzi
3.  Přes angular/cli pomocí příkazu ```ng new <jmeno aplikace>``` vytvoříme novou aplikaci
    Případné otázky na nastavení routování HammerJS a podobné potvrdíme.
4.  Právě jsme vygenerovali novou čistou aplikaci v podobě stejnojmené složky jako je název projektu.
    Teď je potřeba vstoupit do složky projektu a nahradit složku app, style.css a package.json za odevzdané soubory
5.  Aplikace stále ještě nebude fungovat protože je potřeba doinstalovat potřebné knihovny.
    Toho dosáhneme jednoduše spuštěním příkazu ```npm install``` který automaticky podle souboru package.json doinstaluje všechny potřebné knihovny které aplikace požaduje.
6.  Zbývá pouze spuštění. Na to slouží příkaz ```ng serve --o``` který aplikaci přeloží a spustí.
    --o má za následek že po překladu aplikace se otevře localhost na portu 4200 na kterém angular poslouchá, ve vašem výchozím prohlížeči.


Použité knihovny:
Kromě interních knihoven které používá angular jsme použili rozšíření o:

1. Bootstrap
2. Angular material
3. ng2-charts
4. devextreme-angular (nepoužité)

Použité verze knihoven:
Podrobnější informace o všech knihovnách jsou k dispozici v souboru package.json

1. @angular/cli 8.2.2
2. ng2-charts 2.3.0
3. bootstrap 4.3.1
4. angular material 8.2.3
5. devextreme latest
